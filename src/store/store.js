import Vue from 'vue';
import Vuex from 'vuex';
import random from './modules/random';
import categories from './modules/categories';
import search from './modules/search-text';
import jokeBox from './modules/joke-box';
import user from './modules/user';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {},
    modules: {
        random,
        categories,
        search,
        jokeBox,
        user
    }
});

export default store;

