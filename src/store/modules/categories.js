import Vue from "vue";

const types = {
  SET_CATEGORIES: "SET_CATEGORIES",
  SET_CATEGORISED_JOKE: "SET_CATEGORISED_JOKE",
};

const state = {
  categories: [],
  categorizedJoke: "",
};

const mutations = {
  [types.SET_CATEGORIES](state, payload) {
    state.categories = payload;
  },
  [types.SET_CATEGORISED_JOKE](state, payload) {
    state.categorizedJoke = payload;
  },
};

const actions = {
  async fetchCategories({ commit }) {
    await Vue.http.get("categories")
      .then((respose) => {
        return respose.json();
      })
      .then((data) => {
        commit(types.SET_CATEGORIES, data);
      });
  },
  fetchJokeByCategory(context, category) {
    Vue.http
      .get("random", {
        params: {
          category: category,
        },
      })
      .then((respose) => {
        return respose.json();
      })
      .then((data) => {
        context.commit(types.SET_CATEGORISED_JOKE, data);
      });
  },
};

const getters = {
  categories: (state) => state.categories,
  joke: (state) => state.categorizedJoke,
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
