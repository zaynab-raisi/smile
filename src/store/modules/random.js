import Vue from 'vue';

const types = {
    SET_RANDOME_JOKE: 'SET_RANDOME_JOKE'
};

const state = {
    randomJoke: ""
};

const getters = {
    randomJoke: (state) => state.randomJoke
};

const mutations = {
    [types.SET_RANDOME_JOKE](state, payload) {
        state.randomJoke = payload;
    }
};

const actions = {
    async fetchRandomJoke({commit}) {
        await Vue.http.get("random")
        .then(respose => {
            return respose.json();
        }).then(data => {
            commit(types.SET_RANDOME_JOKE, data);
        })
    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}