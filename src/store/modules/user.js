import Vue from 'vue';

const types = {
    SET_IP: 'SET_IP',
    SET_USER_SCORES: 'SET_USER_SCORES'
};

const state = {
    ip: '',
    userScores: {}
};

const getters = {
    ip: (state) => state.ip,
    userScores: (state) => state.userScores
};

const mutations = {
    [types.SET_IP](state, payload) {
        state.ip = payload.ip.replace(/\./g, '');
    }, 
    [types.SET_USER_SCORES](state, payload) {
        state.userScores = payload;
    }
};

const actions = {
    async fetchUserInfo({commit}) {
        await Vue.http.get(`https://api.ipify.org?format=json`)
        .then(response => {
          return response.json()
        })
        .then(data => {
          commit(types.SET_IP, data);
        })
    },

    async fetchUserScores(context, ip) {
        await Vue.http.get(`https://user-cc151.firebaseio.com/${ip}.json`)
        .then(response => {
            //console.log(response);
            return response.json();
        }).then(data => {
            if (data) {
                context.commit(types.SET_USER_SCORES, data);
            }
        })
    },

    async postUserScoreForeJoke(context, data) {
            await Vue.http.put(`https://user-cc151.firebaseio.com/${state.ip}/${data.id}.json`, {how: data.how})
            .then(response => {
                //console.log(response);
            }, error => {
                console.log(error);
            })
    },

    async deleteUserScoreForeJoke(context, id) {
        await Vue.http.delete(`https://user-cc151.firebaseio.com/${state.ip}/${id}.json`)
        .then(response => {
            //console.log(response)
        })
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}