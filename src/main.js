import Vue from 'vue';
import VueRouter from "vue-router";
import VueResource from 'vue-resource';
import 'babel-polyfill';
import App from './App.vue';
import './assets/styles/main.scss';
import routes from './router';
import store from './store/store';

Vue.use(VueRouter);
Vue.use(VueResource);

Vue.http.options.root = "https://api.chucknorris.io/jokes/";

const router = new VueRouter({
  routes: routes,
  mode: 'history'
})

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
