import Vue from 'vue';

const types = {
    SET_SCORES: 'SET_SCORES',
    SET_GOOD_SCORE: 'SET_GOOD_SCORE',
    SET_BAD_SCORE: 'SET_BAD_SCORE'
};

const state = {
    scores: {}
};

const getters = {
    scores: (state) => state.scores
};

const mutations = {
    [types.SET_SCORES](state, payload) {
        if (!payload.data) {
          state.scores = Object.assign({}, state.scores, {
              [payload.joke.id]: { good: 0, bad: 0 }
          });
        } else {
          state.scores = Object.assign({}, state.scores, {
              [payload.joke.id]: payload.data 
            });
        }
    },

    [types.SET_GOOD_SCORE](state, payload) {
        state.scores[payload.id].good += payload.num;
      },

      [types.SET_BAD_SCORE](state, payload) {
        state.scores[payload.id].bad += payload.num;
      }
};

const actions = {
    async fetchScores(context, joke) {
        await Vue.http.get(`https://test-16eec.firebaseio.com/${joke.id}.json`)
        .then(response => {
          //console.log(response);
          return response.json()
        })
        .then(data => {
          context.commit(types.SET_SCORES, {data, joke});
        })
    },

    async postGoodScore(context, data) {
      await Vue.http.put(`https://test-16eec.firebaseio.com/${data.id}.json`, {
        bad: state.scores[data.id].bad, 
        good: state.scores[data.id].good + data.num
      })
      .then(
        response => {
          //console.log(response)
          return response.json();
        },
        error => {
          console.log(error);
        }
      )
    },

    async postBadScore(context, data) {
      await Vue.http.put(`https://test-16eec.firebaseio.com/${data.id}.json`, {
        bad: state.scores[data.id].bad + data.num, 
        good: state.scores[data.id].good
      })
      .then(
        response => {
          //console.log(response)
          return response.json();
        },
        error => {
          console.log(error);
        }
      );
    },

    async switchBetweenGoodBad(context, data) {
      await Vue.http.put(`https://test-16eec.firebaseio.com/${data.id}.json`, data.body)
      .then(
        response => {
          //console.log(response)
          return response.json();
        },
        error => {
          console.log(error);
        }
      );
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}