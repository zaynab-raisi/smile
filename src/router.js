// import Home from "./pages/home";
// import About from './pages/about';
// import Contact from './pages/contact';
// import Random from './pages/random';
// import Categories from './pages/categories';
// import Search from './pages/search-text';

/*var Home = resolve => {
    require.ensure(['./pages/home'], () => {
        resolve(require('./pages/home'));
    });
};

var About = resolve => {
    require.ensure(['./pages/about'], () => {
        resolve(require('./pages/about'));
    });
};

var Contact = resolve => {
    require.ensure(['./pages/contact'], () => {
        resolve(require('./pages/contact'));
    });
};

var Random = resolve => {
    require.ensure(['./pages/random'], () => {
        resolve(require('./pages/random'));
    });
};

var Categories = resolve => {
    require.ensure(['./pages/categories'], () => {
        resolve(require('./pages/categories'));
    });
};

var Search = resolve => {
    require.ensure(['./pages/search-text'], () => {
        resolve(require('./pages/search-text'));
    });
}; */

const Home = () => System.import('./pages/home');
const About = () => System.import('./pages/about');
const Contact = () => System.import('./pages/contact');
const Random = () => System.import('./pages/random');
const Categories = () => System.import('./pages/categories');
const Search = () => System.import('./pages/search-text');

const routes = [
    {
        path: '/',
        component: Home
    },
    {
        path: '/about',
        component: About
    },
    {
        path: '/contact',
        component: Contact
    },
    {
        path: '/random',
        component: Random
    },
    {
        path: '/categories',
        component: Categories
    }, 
    {
        path: '/search',
        component: Search
    }
];

export default routes;