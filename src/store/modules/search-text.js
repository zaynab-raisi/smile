import Vue from 'vue';

const types = {
    SET_TEXT_SEARCH: 'TEXT_SEARCH',
    SET_SEARCHED_JOKES: 'SEARCHED_JOKES'
};

const state = {
    textSearch: "",
    searchedJokes: []
};

const getters = {
    getTextSearch: (state) => state.textSearch,
    getSearchedJokes: (state) => state.searchedJokes
};

const mutations = {
    [types.SET_TEXT_SEARCH](state, payload) {
        state.textSearch = payload;
    },
    [types.SET_SEARCHED_JOKES](state, payload) {
        state.searchedJokes = payload.result;
    }
};

const actions = {
    async fetchSearchedJokes(context, text) {
        await Vue.http.get("search", {
            params: {
                query: text
            }
        })
        .then(respose => {
            return respose.json();
        }).then(data => {
            context.commit(types.SET_SEARCHED_JOKES, data);
        })
    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}